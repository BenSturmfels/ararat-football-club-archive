<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Ararat Football Club</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Ben Sturmfels" />
<meta name="description" content="Information, stats and history about the club and its players." />
<meta name="keywords" content="ararat football club, afc, ararat football, footy, ararat, football, ararat netball club, netball, stats, history, statistics, photos" />
<meta name="distribution" content="global" />
<link rel="stylesheet" title="Simple stuff for dud browsers" href="simple.css" type="text/css" media="screen" />
<style type="text/css">
<!--
@import "complex.css";
-->
</style>
<link rel="stylesheet" title="Print me up" type="text/css" media="print" href="print.css" />
</head>

<body bgcolor="#FFFFFF"  text="#000000" alink="#3366FF" vlink="#990066" link="#990066">
<img src="images/ratlogo.gif" width="89" height="127" alt="Rats Logo" style="float: left;"/>
<h1 style="text-align: left;">Ararat Football Club</h1>
<p style="width: 50%;">This website has been temporarily removed due to funding issues within the club. For more information please <a href="mailto://spaldom@netconnect.com.au">contact Michael Spalding</a>.</p>
</body>
</html>
