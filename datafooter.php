  <!--Non-CSS border-->
  <tr class="retro">
    <td colspan="3" align="center">
		<br /><hr size="1" width="100%" />
	</td>
  </tr>
  <tr> 
<!--Sponsor footer column-->
    <td valign="middle" align="center" width="170">
<div class="sponsor" align="center">Primary sponsor:</div>
<a href="http://www.gason.com.au" title="A.F. Gason Pty. Ltd. - Design &amp; Engineering Manufacturers"><img src="../../images/gasonlogo.gif" width="108" height="56" border="0" vspace="5" hspace="5" alt="Gason Logo" /></a>
    </td>
	<!--Copyright footer--> 
    <td valign="middle" width="99%"><div class="footer">No material from this site may be reproduced without written permission from the <a href="mailto://info@afc.ararat.net.au">Ararat Football Club</a>.<br />Website production by <a href="http://www.stumbles.id.au/design/">Ben</a></div>
    </td>
    <td width="194"><div class="webstds"><img src="../../images/spacer.gif" width="194" height="1" border="0" alt="spacer" /><br />This site is accessable to all browsers, but will look its best in one that supports <a href="http://www.webstandards.org/upgrade/" title="Web Standards Projoct">web standards</a>.</div></td>
  </tr>