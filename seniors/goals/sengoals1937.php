<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>AFC Seniors: Goalkickers of 1937</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Ben Sturmfels" />
<meta name="distribution" content="local" />
<link rel="stylesheet" href="../../simple.css" type="text/css" />
<style type="text/css">
<!--
@import "../../complex.css";
@import "../../data.css";
@import "../../goals.css";
-->
</style>
<link rel="stylesheet" title="Print me up" type="text/css" media="print" href="../../print.css" />
</head>

<body bgcolor="#ffffff" text="#000000" alink="#3366ff" vlink="#990066" link="#990066">
<!--Header table-->
<table id="masthead" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <!--Rat logo--> 
    <td width="89"><img src="../../images/ratlogo.gif" width="89" height="127" alt="Rats Logo" /></td>
    <!--Banner-->
    <td width="463"><img src="../../images/title-graphic.jpg" width="463" height="127" alt="Gason Rats" /></td>
    <!--Section links-->
    <td bgcolor="#FF0000" valign="top" width="99%">
<div class="section" align="right"><a href="../../index.php" title="The main title page">Home</a></div>
<div class="section" align="right"><a href="../../seniors/index.html" title="Senior League - Firsts">Seniors</a></div>
<div class="section" align="right"><a href="../../reserves/index.html" title="Reserve League - Soconds">Reserves</a></div>
<div class="section" align="right"><a href="../../thirds/index.html" title="Thirds">3rds, </a><a href="../../fourths/index.html" title="Fourths">4ths</a></div>
<div class="section" align="right"><a href="../../history/index.html" title="The background &amp; developement of the Club">Club History</a></div>
<div class="section" align="right"><a href="../../netball/index.html" title="Ararat Netball Club">Netball</a></div>
</td>
  </tr>
  <!--Title graphic-->
  <tr bgcolor="#ff0000"> 
    <td colspan="3"><h1><img src="../../images/ararat-footy-title.gif" width="380" height="30" alt="Ararat Football Club" /></h1></td>
  </tr>
</table>
<!--Content table-->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <!--Menu column-->
    <td id="menucol" width="170" valign="top">
	  <div class="menu"><a href="../players/index.html">Past Players</a></div>
          <div class="menu"><a href="../capt-coach/index.html">Coach/Captain</a></div>
	  <div class="menu"><a href="../afc-bf/index.html">AFC Best &amp; Fairest</a></div>
	  <div class="menu"><a href="../wfl-bf/index.html">WFL Best &amp; Fairest</a></div>
          <div class="menuselected"><a href="../goals/index.html">Goalkickers</a></div>
<ul class="submenu">
      <li><a href="sengoals1938.php">Forward one year</a></li>
<!--      <li><a href="sengoals1936.php">Back one year</a></li>-->
</ul>	  <div class="menu"><a href="../goalkickers/index.html">Leading Goalkickers</a></div><div class="menu"><a href="../gf-teams/index.html">Grand Final Teams</a></div>
	  <div class="menu"><a href="../vfl-afl/index.html">VFL/AFL Players</a></div>
	  <div class="menu"><a href="../interleague/index.html">Interleague</a></div>
	  <div class="menu"><a href="../opposition/index.html">Opposition</a></div>	  <img src="../../images/spacer.gif" width="170" height="1" border="0" alt="&nbsp;" />
<br class="retro" />
<div class="note"><strong>Note:<br /></strong>
<?php  include 'listofopposition.php';
?>
</div>

    </td>
	<!--Main content column-->
    <td id="contentcol" colspan="2">
 <h2>Seniors</h2>
<h3><strong>Goal kickers</strong> for each round of <strong>1937</strong></h3>
<table id="data" border="1">
<tr class="roundsrow">
<th>&nbsp;</th>
<th class="roundh">Round:</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>12</th>
<th>13</th>
<th>14</th>
<th>15</th>
<th>16</th>
<th>&nbsp;</th>
</tr>

<tr class="opprow">
<th class="playerh">PLAYER</th>
<th class="opphead">Opponent:</th>
<th>d</th>
<th>n</th>
<th>mi</th>
<th>s</th>
<th>h</th>
<th>mu</th>
<th>w</th>
<th>j</th>
<th>d</th>
<th>n</th>
<th>mi</th>
<th>s</th>
<th>h</th>
<th>mu</th>
<th>w</th>
<th>j</th>
<th>&nbsp;</th>
</tr>

<tr class="or">
<td class="surname">KENNEDY</td>
<td class="firstname">Dan</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>6</td>
<td>4</td>
<td>&nbsp;</td>
<td>9</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(23)</td>
</tr>

<tr class="er">
<td class="surname">BRODERICK</td>
<td class="firstname">Vin</td>
<td class="oc">3</td>
<td>3</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">5</td>
<td>&nbsp;</td>
<td class="ptotalo">(13)</td>
</tr>

<tr class="or">
<td class="surname">FELTSCHEER</td>
<td class="firstname">Eric</td>
<td>2</td>
<td>2</td>
<td>&nbsp;</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>3</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td class="ptotalo">(13)</td>
</tr>

<tr class="er">
<td class="surname">MILLER</td>
<td class="firstname">George</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>5</td>
<td class="ptotalo">(10)</td>
</tr>

<tr class="or">
<td class="surname">O'ROURKE</td>
<td class="firstname">Frank</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>6</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(10)</td>
</tr>

<tr class="er">
<td class="surname">CLOUGH</td>
<td class="firstname">Eddie</td>
<td class="oc">2</td>
<td>2</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(9)</td>
</tr>

<tr class="or">
<td class="surname">McRAE</td>
<td class="firstname">Aub</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td class="ptotalo">(8)</td>
</tr>

<tr class="er">
<td class="surname">McDOUGALL</td>
<td class="firstname">Keith</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="ptotalo">(7)</td>
</tr>

<tr class="or">
<td class="surname">JAENSCH</td>
<td class="firstname">Bert</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>4</td>
<td>&nbsp;</td>
<td>2</td>
<td class="ptotalo">(7)</td>
</tr>

<tr class="er">
<td class="surname">ROWE</td>
<td class="firstname">S</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="ptotalo">(7)</td>
</tr>

<tr class="or">
<td class="surname">BICKNELL</td>
<td class="firstname">Des</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="er">
<td class="surname">DUNSTAN</td>
<td class="firstname">Jack</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>3</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="or">
<td class="surname">SMART</td>
<td class="firstname">Syd</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="er">
<td class="surname">McDOUGALL</td>
<td class="firstname">James</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="or">
<td class="surname">GILLETT</td>
<td class="firstname">Harold</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="er">
<td class="surname">LARDNER</td>
<td class="firstname">Lin</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="or">
<td class="surname">PETERSON</td>
<td class="firstname">Frank</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="er">
<td class="surname">MOYSEY</td>
<td class="firstname">Jack</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="or">
<td class="surname">ROBINSON</td>
<td class="firstname">D</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="er">
<td class="surname">SULLIVAN</td>
<td class="firstname">Dennis</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="or">
<td class="surname">TURNBULL</td>
<td class="firstname">William</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="er">
<td class="surname">CANDY</td>
<td class="firstname">Merv</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="or">
<td class="surname">BLOCK</td>
<td class="firstname">Jim</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="ttotalsrow">
<th>&nbsp;</th>
<th class="totalsh">Totals:</th>
<th>10</th>
<th>9</th>
<th>7</th>
<th>10</th>
<th>6</th>
<th>6</th>
<th>10</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>6</th>
<th>7</th>
<th>9</th>
<th>7</th>
<th>9</th>
<th>16</th>
<th>&nbsp;</th>
</tr>
</table>
        </td>	
  </tr>

<?php
  include '../../datafooter.php';
?>

</table>
</body>
</html>

