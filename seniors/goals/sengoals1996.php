<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>AFC Seniors: Goalkickers of 1996</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Ben Sturmfels" />
<meta name="distribution" content="local" />
<link rel="stylesheet" href="../../simple.css" type="text/css" />
<style type="text/css">
<!--
@import "../../complex.css";
@import "../../data.css";
@import "../../goals.css";
-->
</style>
<link rel="stylesheet" title="Print me up" type="text/css" media="print" href="../../print.css" />
</head>

<body bgcolor="#ffffff" text="#000000" alink="#3366ff" vlink="#990066" link="#990066">
<!--Header table-->
<table id="masthead" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <!--Rat logo--> 
    <td width="89"><img src="../../images/ratlogo.gif" width="89" height="127" alt="Rats Logo" /></td>
    <!--Banner-->
    <td width="463"><img src="../../images/title-graphic.jpg" width="463" height="127" alt="Gason Rats" /></td>
    <!--Section links-->
    <td bgcolor="#FF0000" valign="top" width="99%">
<div class="section" align="right"><a href="../../index.php" title="The main title page">Home</a></div>
<div class="section" align="right"><a href="../../seniors/index.html" title="Senior League - Firsts">Seniors</a></div>
<div class="section" align="right"><a href="../../reserves/index.html" title="Reserve League - Soconds">Reserves</a></div>
<div class="section" align="right"><a href="../../thirds/index.html" title="Thirds">3rds, </a><a href="../../fourths/index.html" title="Fourths">4ths</a></div>
<div class="section" align="right"><a href="../../history/index.html" title="The background &amp; developement of the Club">Club History</a></div>
<div class="section" align="right"><a href="../../netball/index.html" title="Ararat Netball Club">Netball</a></div>
</td>
  </tr>
  <!--Title graphic-->
  <tr bgcolor="#ff0000"> 
    <td colspan="3"><h1><img src="../../images/ararat-footy-title.gif" width="380" height="30" alt="Ararat Football Club" /></h1></td>
  </tr>
</table>
<!--Content table-->
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <!--Menu column-->
    <td id="menucol" width="170" valign="top">
	  <div class="menu"><a href="../players/index.html">Past Players</a></div>
          <div class="menu"><a href="../capt-coach/index.html">Coach/Captain</a></div>
	  <div class="menu"><a href="../afc-bf/index.html">AFC Best &amp; Fairest</a></div>
	  <div class="menu"><a href="../wfl-bf/index.html">WFL Best &amp; Fairest</a></div>
          <div class="menuselected"><a href="../goals/index.html">Goalkickers</a></div>
<ul class="submenu">
      <li><a href="sengoals1997.php">Forward one year</a></li>
      <li><a href="sengoals1995.php">Back one year</a></li>
</ul>	  <div class="menu"><a href="../goalkickers/index.html">Leading Goalkickers</a></div><div class="menu"><a href="../gf-teams/index.html">Grand Final Teams</a></div>
	  <div class="menu"><a href="../vfl-afl/index.html">VFL/AFL Players</a></div>
	  <div class="menu"><a href="../interleague/index.html">Interleague</a></div>
	  <div class="menu"><a href="../opposition/index.html">Opposition</a></div>	  <img src="../../images/spacer.gif" width="170" height="1" border="0" alt="&nbsp;" />
<br class="retro" />
<div class="note"><strong>Note:<br /></strong>
<?php  include 'listofopposition.php';
?>
</div>

    </td>
	<!--Main content column-->
    <td id="contentcol" colspan="2">
 <h2>Seniors</h2>
<h3><strong>Goal kickers</strong> for each round of <strong>1996</strong></h3>
<table id="data" border="1">
<tr class="roundsrow">
<th>&nbsp;</th>
<th class="roundh">Round:</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>12</th>
<th>13</th>
<th>14</th>
<th>15</th>
<th>16</th>
<th>ef</th>
<th>sf</th>
<th>&nbsp;</th>
</tr>

<tr class="opprow">
<th class="playerh">PLAYER</th>
<th class="opphead">Opponent:</th>
<th>hu</th>
<th>w</th>
<th>mm</th>
<th>d</th>
<th>h</th>
<th>n</th>
<th>s</th>
<th>sm</th>
<th>hu</th>
<th>w</th>
<th>mm</th>
<th>d</th>
<th>h</th>
<th>n</th>
<th>s</th>
<th>sm</th>
<th>h</th>
<th>sm</th>
<th>&nbsp;</th>
</tr>

<tr class="or">
<td class="surname">BRYAN</td>
<td class="firstname">Brendan</td>
<td>&nbsp;</td>
<td>5</td>
<td>2</td>
<td>1</td>
<td>6</td>
<td>1</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>3</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td class="ptotalo">(30)</td>
</tr>

<tr class="er">
<td class="surname">HITCHCOCK</td>
<td class="firstname">Shane</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>9</td>
<td class="oc">1</td>
<td>5</td>
<td class="oc">6</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="ptotalo">(26)</td>
</tr>

<tr class="or">
<td class="surname">SHEA</td>
<td class="firstname">Tim</td>
<td>&nbsp;</td>
<td>2</td>
<td>3</td>
<td>3</td>
<td>1</td>
<td>1</td>
<td>3</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>3</td>
<td>1</td>
<td class="ptotalo">(25)</td>
</tr>

<tr class="er">
<td class="surname">WILLIAMSON</td>
<td class="firstname">Allister</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">1</td>
<td>2</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>2</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">3</td>
<td>5</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(24)</td>
</tr>

<tr class="or">
<td class="surname">KEILAR</td>
<td class="firstname">Terry</td>
<td>3</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>3</td>
<td>&nbsp;</td>
<td class="ptotalo">(20)</td>
</tr>

<tr class="er">
<td class="surname">SUMMERS</td>
<td class="firstname">Matthew</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">2</td>
<td>4</td>
<td class="oc">2</td>
<td>2</td>
<td class="oc">4</td>
<td>&nbsp;</td>
<td class="ptotalo">(20)</td>
</tr>

<tr class="or">
<td class="surname">WALTON</td>
<td class="firstname">Doug</td>
<td>&nbsp;</td>
<td>3</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>3</td>
<td>1</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(19)</td>
</tr>

<tr class="er">
<td class="surname">ALLGOOD</td>
<td class="firstname">Shane</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">3</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">1</td>
<td>1</td>
<td class="ptotalo">(13)</td>
</tr>

<tr class="or">
<td class="surname">HARVEY</td>
<td class="firstname">Jason</td>
<td>&nbsp;</td>
<td>4</td>
<td>&nbsp;</td>
<td>4</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td class="ptotalo">(11)</td>
</tr>

<tr class="er">
<td class="surname">ANTONIO</td>
<td class="firstname">Glenn</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>2</td>
<td class="ptotalo">(10)</td>
</tr>

<tr class="or">
<td class="surname">GRAHAM</td>
<td class="firstname">Scott</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>&nbsp;</td>
<td class="ptotalo">(10)</td>
</tr>

<tr class="er">
<td class="surname">DOHNT</td>
<td class="firstname">Adrian</td>
<td class="oc">3</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(8)</td>
</tr>

<tr class="or">
<td class="surname">FOX</td>
<td class="firstname">Mark</td>
<td>1</td>
<td>3</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="er">
<td class="surname">COOPER</td>
<td class="firstname">Craig</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>3</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="or">
<td class="surname">BAHL</td>
<td class="firstname">Darren</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>&nbsp;</td>
<td class="ptotalo">(6)</td>
</tr>

<tr class="er">
<td class="surname">REID</td>
<td class="firstname">Chris</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">2</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="or">
<td class="surname">SOMERS</td>
<td class="firstname">Craig</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>&nbsp;</td>
<td class="ptotalo">(4)</td>
</tr>

<tr class="er">
<td class="surname">CLARK</td>
<td class="firstname">Aaron</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>2</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="or">
<td class="surname">FLEMING</td>
<td class="firstname">Adam</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="er">
<td class="surname">HOLZ</td>
<td class="firstname">Jake</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>1</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="ptotalo">(3)</td>
</tr>

<tr class="or">
<td class="surname">SMART</td>
<td class="firstname">Shaun</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(2)</td>
</tr>

<tr class="er">
<td class="surname">CARROLL</td>
<td class="firstname">Jason</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="or">
<td class="surname">CORBETT</td>
<td class="firstname">Gerard</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="er">
<td class="surname">HOGAN</td>
<td class="firstname">Dean</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="or">
<td class="surname">MOLONY</td>
<td class="firstname">Bart</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>1</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="er">
<td class="surname">HOGAN</td>
<td class="firstname">Mark</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">&nbsp;</td>
<td>&nbsp;</td>
<td class="oc">1</td>
<td>&nbsp;</td>
<td class="ptotalo">(1)</td>
</tr>

<tr class="ttotalsrow">
<th>&nbsp;</th>
<th class="totalsh">Totals:</th>
<th>11</th>
<th>25</th>
<th>11</th>
<th>29</th>
<th>18</th>
<th>12</th>
<th>21</th>
<th>8</th>
<th>14</th>
<th>10</th>
<th>12</th>
<th>12</th>
<th>13</th>
<th>19</th>
<th>8</th>
<th>7</th>
<th>21</th>
<th>7</th>
<th>&nbsp;</th>
</tr>
</table>
        </td>	
  </tr>

<?php
  include '../../datafooter.php';
?>

</table>
</body>
</html>

